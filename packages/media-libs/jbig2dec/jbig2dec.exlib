# Copyright 2011 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=ArtifexSoftware release=${PV} suffix=tar.gz ]

export_exlib_phases src_prepare

TEST_REV="f7d5087d3d6c236707842dcd428818c6cb8fb041"

SUMMARY="Decoder implementation of the JBIG2 image compression format"
DESCRIPTION="
jbig2dec is a decoder implementation of the JBIG2 image compression format. JBIG2 is designed for
lossy or lossless encoding of 'bilevel' (1-bit monochrome) images at moderately high resolution, and
in particular scanned paper documents. In this domain it is very efficient, offering compression
ratios on the order of 100:1.
"
DOWNLOADS+="
    https://github.com/ArtifexSoftware/tests/raw/${TEST_REV}/jbig2/jb2streams.zip -> ${PNV}-jb2streams-${TEST_REV}.zip
"

LICENCES="AGPL-3"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        media-libs/libpng:=
    test:
        dev-lang/python:*[>=3]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-shared
    --disable-static
)

jbig2dec_src_prepare() {
    # Move test files where test_jbig2dec.py will find them
    edo mkdir -p "${WORK}"/tests/ubc
    edo mv "${WORKBASE}"/*.{bmp,jb2} "${WORK}"/tests/ubc
}

