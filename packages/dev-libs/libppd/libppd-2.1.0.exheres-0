# Copyright 2023-2025 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=OpenPrinting release=${PV} suffix=tar.xz ]

SUMMARY="libppd library"
DESCRIPTION="
Legacy PPD file support. To avoid re-writing support for an obsolete format from scratch, only for
retro-fitting legacy printer drivers we have created this package, libppd, the legacy support
library for PPD files, which is by 95 % code overtaken from CUPS 2.x.
"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.18.3]
        virtual/pkg-config
    build+run:
        app-text/ghostscript
        app-text/poppler
        dev-libs/libcupsfilters[>=2.0.0]
        net-print/cups[>=2.4.7-r2]
        sys-libs/zlib
"

src_configure() {
    local myconf=(
        --enable-genstrings
        --enable-ghostscript
        --enable-gs-ps2write
        --enable-nls
        --enable-ppdc-utils
        --enable-testppdfile
        --disable-acroread
        --disable-debug-guards
        --disable-debug-printfs
        --disable-mutool
        --disable-pdftocairo
        --disable-static
        --disable-werror
        --with-cups-config=/usr/$(exhost --target)/bin/cups-config
        --with-cups-rundir=/run/cups
        --with-ippfind-path=system
        --with-pdftops=hybrid
        --with-pdftops-maxres=1440
        --with-pdftops-path=system
    )

    econf "${myconf[@]}"
}

