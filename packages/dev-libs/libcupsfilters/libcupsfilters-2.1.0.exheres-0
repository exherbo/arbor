# Copyright 2023-2025 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=OpenPrinting release=${PV} suffix=tar.xz ]

SUMMARY="libcupsfilters library"
DESCRIPTION="
This package provides the libcupsfilters library, which in its 2.x version contains all the code of
the filters of the former cups-filters package as library functions, the so-called filter
functions.
"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    dbus
    tiff
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.18.3]
        virtual/pkg-config
    build+run:
        app-text/ghostscript
        app-text/poppler[>=0.19][lcms]
        app-text/qpdf[>=11.0.0]
        media-libs/fontconfig[>=2.0.0]
        media-libs/freetype:2
        media-libs/lcms2
        media-libs/libexif
        media-libs/libpng:=
        net-print/cups
        dbus? ( sys-apps/dbus )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        tiff? ( media-libs/tiff:= )
    test:
        fonts/dejavu
"

src_configure() {
    local myconf=(
        --enable-exif
        --enable-ghostscript
        --enable-imagefilters
        --enable-nls
        --enable-poppler
        --disable-mutool
        --disable-static
        --disable-werror
        --with-cups-config=/usr/$(exhost --target)/bin/cups-config
        --with-cups-rundir=/run/cups
        --with-ippfind-path=system
        --with-jpeg
        --with-png
        --with-test-font-path=/usr/share/fonts/X11/dejavu/DejaVuSans.ttf
        $(option_enable dbus)
        $(option_with tiff)
    )

    econf "${myconf[@]}"
}

