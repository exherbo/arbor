# Copyright 2009, 2010 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require python-build

PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"

DEPENDENCIES+="
    build+run:
        app-arch/xz
"

src_test() {
    local DISABLE_TESTS
    DISABLE_TESTS=(
        # Has problems with path resolution
        test_distutils
        test_peg_generator

        # Never-ending threads running under gdb
        test_gdb

        # Run forever sometimes (sandboxing issue?)
        test_multiprocessing_fork
        test_multiprocessing_forkserver
        test_multiprocessing_spawn

        # Seems to hang
        test_socket

        # Fail in CI environment
        test_asyncore
        test_posix

        # Want access to DNS
        test_robotparser
        test_smtpnet
        test_ssl
        test_timeout
        test_urllib
        test_urllibnet
        test_urllib2
        test_urllib2_localnet
        test_urllib2net

        # Want to bind to 0.0.0.0/::
        # TODO: check which of these tests actually fall in this category
        test_asyncio
        test_ctypes
        test_fcntl
        test_float
        test_ftplib
        test_httplib
        test_normalization
        test_os
        test_pathlib
        test_pydoc
        test_pyexpat
        test_site
        test_sqlite

        # Downloads data from the Internet
        test_unicodedata
    )

    # Disable tests that fail with musl
    if [[ $(exhost --target) == *-*-linux-musl* ]]; then
        DISABLE_TESTS+=(
            test__locale
            test_c_locale_coercion
            test_locale
            test_re
            test_threading
        )
    fi

    python-build_src_test
}

