# Copyright 2012 Morgane "Sardem FF7" Glidic <sardemff7@exherbo.org>
# Copyright 2023 Maxime Sorin <maxime.sorin@clever-cloud.com>
# Distributed under the terms of the GNU General Public License v2

require alternatives systemd-service github [ project=otp tag="OTP-${PV}" ]
require wxwidgets [ with_opt=true ]

export_exlib_phases src_prepare src_install

myexparam pv=$(ever replace 1 B $(ever replace 2 - ${PV}))

MY_PNV=otp_src_${PV}

SUMMARY="A language to build massively scalable soft real-time systems with requirements on high availability"
HOMEPAGE="https://www.erlang.org/"

LICENCES="EPL-1.1"
SLOT="$(ever major)"

MYOPTIONS="odbc systemd
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
    build+run:
        sys-libs/ncurses
        sys-libs/zlib
        odbc? ( dev-db/unixODBC )
        systemd? ( sys-apps/systemd )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        wxwidgets? ( x11-dri/glu )
        !dev-lang/erlang:0 [[
            description = [ Erlang is now slotted ]
            resolution = uninstall-blocked-before
        ]]
        group/epmd
        user/epmd
"

if ever at_least 25 ; then
    DEPENDENCIES+="
        build:
            wxwidgets? ( virtual/pkg-config )
        build+run:
            wxwidgets? ( dev-libs/glib:2 )
    "
fi

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --hates=docdir
    --hates=datarootdir
    --enable-dynamic-ssl-lib
    --enable-kernel-poll
    --enable-shared-zlib
    --enable-smp-support
    --enable-threads
    --with-ssl
    --with-termcap
    --without-javac
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( systemd )

erlang_src_prepare() {
    default

    option odbc || echo "odbc" >> lib/SKIP-APPLICATIONS
    option wxwidgets || echo "wx" >> lib/SKIP-APPLICATIONS

    # fix pkg-config, patch configure directly as I couldn't get autotools to work
    if ever at_least 25 ; then
        edo sed \
            -e "s:pkg-config:${PKG_CONFIG}:g" \
            -i lib/wx/configure{,.ac}
    fi
}

DEFAULT_SRC_INSTALL_PARAMS=( libdir_suffix="/erlang-${SLOT}" )

erlang_src_install() {
    default

    local alternatives=()

    edo pushd "${IMAGE}"/usr/$(exhost --target)/bin

    for bin in *; do
	alternatives+=( "/usr/$(exhost --target)/bin/${bin}" ${bin}-${SLOT} )
    done

    edo popd

    edo find "${IMAGE}" -type d -empty -exec rmdir '{}' +

    alternatives_for ${PN} ${SLOT} ${SLOT} "${alternatives[@]}"

    install_systemd_files
}

