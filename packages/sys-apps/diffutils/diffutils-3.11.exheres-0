# Copyright 2007 Bryan Østergaard
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=tar.xz ]

if [[ $(exhost --target) == *-musl* ]] ; then
    # "${FILES}"/${PN}-skip-some-gnulib-tests.patch
    require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]
fi

SUMMARY="Utilities for showing file differences in various ways"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-devel/gettext
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    ac_cv_libsigsegv=no # Do not link against libsigsegv
    # Fix cross compiling with glibc[>=2.26], don't use the bundled getopt_long
    # replacement, it will conflict with the one from glibc.
    gl_cv_func_getopt_gnu=yes
)

src_prepare() {
    if [[ $(exhost --target) == *-musl* ]] ; then
        expatch "${FILES}"/${PN}-skip-some-gnulib-tests.patch
        autotools_src_prepare
    else
        default
    fi
}

src_test() {
    # Needed for the bundled gnulib tests
    esandbox allow_net "inet:127.0.0.1@80"
    esandbox allow_net --connect "inet:127.0.0.1@80"

    emake check

    esandbox disallow_net --connect "inet:127.0.0.1@80"
    esandbox disallow_net "inet:127.0.0.1@80"
}

