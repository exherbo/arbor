From 68c170d1bda13f5c556778aae49e7643c4c84466 Mon Sep 17 00:00:00 2001
From: Marvin Schmidt <marv@exherbo.org>
Date: Thu, 15 Sep 2022 04:37:36 +0200
Subject: [PATCH] tests: Skip tests that don't work with musl

As upstream already notes in the test files:

> Note: This test fails on Linux with musl libc versions that don't support
> the BIG5 encoding in 'iconv'.

So it's only logical to skip them when we build on musl systems

Original patch from Alpine Linux:
https://gitlab.alpinelinux.org/alpine/aports/-/blob/b4520ee089db4d4ec2fa527dfa70ed07fb268811/main/gettext/skip-tests-musl.patch
---
 gettext-tools/tests/format-c-5            | 4 ++++
 gettext-tools/tests/lang-bash             | 3 +++
 gettext-tools/tests/lang-sh               | 3 +++
 gettext-tools/tests/msgcat-22             | 2 ++
 gettext-tools/tests/msgconv-2             | 2 ++
 gettext-tools/tests/msgconv-8             | 2 ++
 gettext-tools/tests/msgmerge-compendium-6 | 2 ++
 gettext-tools/tests/xgettext-python-3     | 2 ++
 8 files changed, 20 insertions(+)

diff --git a/gettext-tools/tests/format-c-5 b/gettext-tools/tests/format-c-5
index 3c64c8794..cfe9f0bd4 100755
--- a/gettext-tools/tests/format-c-5
+++ b/gettext-tools/tests/format-c-5
@@ -67,6 +67,10 @@ if test -z "$USE_SYSTEM_LIBINTL"; then
 else
   # In the system-tests/ dir: Don't use a fake setlocale.
 
+  # The `setlocale` call succeeds with musl libc even if
+  # the fa_IR locale isn't supported, skip this test
+  Exit 77
+
   skipped=true
 
   prepare_locale_ fa fa_IR
diff --git a/gettext-tools/tests/lang-bash b/gettext-tools/tests/lang-bash
index 268ec3264..96fcfb90b 100755
--- a/gettext-tools/tests/lang-bash
+++ b/gettext-tools/tests/lang-bash
@@ -14,6 +14,9 @@
 # because in this case the gettext-runtime/src/gettext program does not do
 # any message catalog lookups.
 
+# See above, we build with --disable-nls on musl
+Exit 77
+
 cat <<\EOF > prog.bash
 #! /bin/bash
 
diff --git a/gettext-tools/tests/lang-sh b/gettext-tools/tests/lang-sh
index afc297323..26cc481c0 100755
--- a/gettext-tools/tests/lang-sh
+++ b/gettext-tools/tests/lang-sh
@@ -12,6 +12,9 @@
 # because in this case the gettext-runtime/src/gettext program does not do
 # any message catalog lookups.
 
+# See above, we build with --disable-nls on musl
+Exit 77
+
 cat <<\EOF > prog.sh
 #! /bin/sh
 
diff --git a/gettext-tools/tests/msgcat-22 b/gettext-tools/tests/msgcat-22
index 604718811..00139ba87 100755
--- a/gettext-tools/tests/msgcat-22
+++ b/gettext-tools/tests/msgcat-22
@@ -6,6 +6,8 @@
 # Note: This test fails on Linux with musl libc versions that don't support
 # the GB18030 encoding in 'iconv'.
 
+Exit 77
+
 cat <<\EOF > mcat-test22.po
 msgid ""
 msgstr ""
diff --git a/gettext-tools/tests/msgconv-2 b/gettext-tools/tests/msgconv-2
index d286cdae5..d96c4873f 100755
--- a/gettext-tools/tests/msgconv-2
+++ b/gettext-tools/tests/msgconv-2
@@ -7,6 +7,8 @@
 # Note: This test fails on Linux with musl libc versions that don't support
 # the BIG5 encoding in 'iconv'.
 
+Exit 77
+
 cat <<\EOF > mco-test2.po
 # Chinese translation for GNU gettext messages.
 #
diff --git a/gettext-tools/tests/msgconv-8 b/gettext-tools/tests/msgconv-8
index 207b0f01e..808386fc8 100755
--- a/gettext-tools/tests/msgconv-8
+++ b/gettext-tools/tests/msgconv-8
@@ -6,6 +6,8 @@
 # Note: This test fails on Linux with musl libc versions that don't support
 # the GB18030 encoding in 'iconv'.
 
+Exit 77
+
 cat <<\EOF > mco-test8.po
 msgid ""
 msgstr ""
diff --git a/gettext-tools/tests/msgmerge-compendium-6 b/gettext-tools/tests/msgmerge-compendium-6
index 59eb00e04..c4be0b20f 100755
--- a/gettext-tools/tests/msgmerge-compendium-6
+++ b/gettext-tools/tests/msgmerge-compendium-6
@@ -10,6 +10,8 @@
 # Note: This test fails on Linux with musl libc versions and on Solaris 11
 # (OpenIndiana, OmniOS) that don't support the EUC-KR encoding in 'iconv'.
 
+Exit 77
+
 : ${MSGCONV=msgconv}
 ${MSGCONV} --to-code=UTF-8 -o mm-ko.utf-8.pot "$wabs_srcdir"/mm-ko.ascii.pot
 
diff --git a/gettext-tools/tests/xgettext-python-3 b/gettext-tools/tests/xgettext-python-3
index 1e13b5793..7cd480d5e 100755
--- a/gettext-tools/tests/xgettext-python-3
+++ b/gettext-tools/tests/xgettext-python-3
@@ -6,6 +6,8 @@
 # Note: This test fails on Linux with musl libc versions that don't support
 # the EUC-JP encoding in 'iconv'.
 
+Exit 77
+
 cat <<\EOF > xg-py-3a.py
 #!/usr/bin/env python
 # TRANSLATORS: Fran���ois Pinard is a hero.
-- 
2.42.0

