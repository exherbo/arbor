# Copyright 2012-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ release=v${PV} suffix=tar.gz ] \
    cmake \
    bash-completion \
    zsh-completion

SUMMARY="C++ library and set of programs that inspect and manipulate the structure of PDF files"
DESCRIPTION="
QPDF is a C++ library and set of programs that inspect and manipulate the
structure of PDF files. It can encrypt and linearize files, expose the
internals of a PDF file, and do many other operations useful to end users and
PDF developers.
"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/files/${PN}-manual.html [[ lang = en ]]"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        sys-libs/zlib
        providers:gnutls? ( dev-libs/gnutls )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=1.1.0] )
    test:
        app-text/ghostscript[>=9.21-r4][tiff]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-libtests-include-cstdint-for-GCC-15.patch
)

BASH_COMPLETIONS=(
    "${CMAKE_SOURCE}"/completions/bash/${PN}
)
ZSH_COMPLETIONS=(
    "${CMAKE_SOURCE}"/completions/zsh/_${PN}
)

src_configure() {
    local cmake_params=(
        -DALLOW_CRYPTO_NATIVE:BOOL=TRUE
        -DBUILD_DOC:BOOL=FALSE
        -DBUILD_DOC_DIST:BOOL=FALSE
        -DBUILD_SHARED_LIBS:BOOL=TRUE
        -DBUILD_STATIC_LIBS:BOOL=FALSE
        -DCHECK_SIZES:BOOL=FALSE
        -DCI_MODE:BOOL=FALSE
        -DCMAKE_INSTALL_DOCDIR:PATH="/usr/share/doc/${PNVR}"
        -DCXX_NEXT:BOOL=FALSE
        -DDEFAULT_CRYPTO:STRING=$(option providers:gnutls gnutls openssl)
        -DENABLE_COVERAGE:BOOL=FALSE
        -DENABLE_QTC:BOOL=FALSE
        -DFUTURE:BOOL=FALSE
        -DGENERATE_AUTO_JOB:BOOL=FALSE
        -DINSTALL_CMAKE_PACKAGE:BOOL=TRUE
        -DINSTALL_EXAMPLES:BOOL=FALSE
        -DINSTALL_MANUAL:BOOL=FALSE
        -DINSTALL_PKGCONFIG:BOOL=TRUE
        -DMAINTAINER_MODE:BOOL=FALSE
        -DOSS_FUZZ:BOOL=FALSE
        -DQTEST_COLOR:BOOL=TRUE
        -DRANDOM_DEVICE:PATH=/dev/urandom
        -DREQUIRE_CRYPTO_GNUTLS:BOOL=$(option providers:gnutls TRUE FALSE)
        -DREQUIRE_CRYPTO_NATIVE:BOOL=FALSE
        -DREQUIRE_CRYPTO_OPENSSL:BOOL=$(option providers:openssl TRUE FALSE)
        -DSHOW_FAILED_TEST_OUTPUT:BOOL=$(expecting_tests TRUE FALSE)
        -DSKIP_OS_SECURE_RANDOM:BOOL=FALSE
        -DUSE_IMPLICIT_CRYPTO:BOOL=FALSE
        -DUSE_INSECURE_RANDOM:BOOL=FALSE
        -DWERROR:BOOL=FALSE
    )

    ecmake \
        "${cmake_params[@]}"
}

src_install() {
    cmake_src_install

    bash-completion_src_install
    zsh-completion_src_install
}

