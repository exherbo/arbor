# Copyright 2009-2016 Wulf C. Krueger
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'pam-1.0.1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require github [ user=linux-pam project=linux-pam release=v${PV} pnv=Linux-PAM-${PV} suffix=tar.xz ] \
    meson \
    systemd-service \
    flag-o-matic \
    pam

export_exlib_phases pkg_pretend src_prepare src_test src_install

SUMMARY="Linux-PAM provides Pluggable Authentication Modules"

LICENCES="|| ( GPL-2 BSD-3 )"
SLOT="0"
MYOPTIONS="
    nis [[ description = [ Network Information Service support ] ]]
    parts: binaries configuration development documentation libraries
    ( providers: elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
    ( providers: openssl libressl ) [[
        *description = [ To provide crypto algorithm for hmac ]
        number-selected = exactly-one
    ]]

    ( libc: musl )
"

DEPENDENCIES="
    build:
        app-text/docbook-xsl-ns-stylesheets
        dev-libs/libxslt [[ note = [ xsltproc ] ]]
        sys-devel/flex
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        !libc:musl? ( dev-libs/libxcrypt:= )
        nis? (
            net-libs/libnsl
            net-libs/libtirpc
        )
    run:
        dev-libs/libpwquality
        sys-apps/shadow[>=4.9] [[ note = [ for yescrypt support ] ]]
        !libc:musl? ( dev-libs/libxcrypt:=[>=4.3] [[ note = [ for yescrypt support ] ]] )
        providers:elogind? ( sys-auth/elogind[pam] )
        providers:systemd? ( sys-apps/systemd[>=254] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
    suggestion:
        providers:elogind? ( sys-auth/pam_rundir ) [[
            description = [ Set XDG_RUNTIME_DIR automatically on login without systemd ]
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    --native-file meson-native-pam.txt
    -Daudit=disabled
    -Ddocdir=/usr/share/doc/${PNVR}
    -Ddocs=enabled
    -Deconf=disabled
    # requires libelogind >= 254 which we do not have yet
    -Delogind=disabled
    -Dexamples=false
    -Dhtmldir=/usr/share/doc/${PNVR}/html
    -Di18n=enabled
    -Dkernel-overflow-uid=65534
    -Dlckpwdf=true
    -Disadir=/usr/$(exhost --target)/lib/security
    -Dopenssl=enabled
    -Dpam-debug=false
    -Dpam_unix=enabled
    -Dpam_userdb=disabled
    -Dpamlocking=false
    -Dpdfdir=/usr/share/doc/${PNVR}/pdf
    -Dread-both-confs=false
    -Dsconfigdir=/etc/security
    -Dsecuredir=/usr/$(exhost --target)/lib/security
    -Dselinux=disabled
    -Dsystemdunitdir=${SYSTEMDSYSTEMUNITDIR}
    -Duidmin=1000
    -Dusergroups=false
    -Dxtests=false
)

MESON_SRC_CONFIGURE_OPTIONS=(
    # pam_lastlog is deprecated and stated to be removed, last checked: 1.7.0
    '!libc:musl -Dpam_lastlog=enabled -Dpam_lastlog=disabled'
    'providers:systemd -Dlogind=enabled -Dlogind=disabled'
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=( nis )

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( doc/txt/* )

pam_build_pkg_pretend() {
    if [[ -z ${PAM_UPDATE} ]] && has_version "${CATEGORY}/${PN}" && \
        ! has_version "${CATEGORY}/${PN}[>=1.5.0]" ; then
        ewarn "As of pam-1.5.0, pam_tally and pam_tally2 have been removed in favour of pam_faillock,"
        ewarn "and pam_cracklib has been removed in favour of pam_pwquality."
        ewarn "Given the removal of these modules could result in you being locked out of your system,"
        ewarn "please ensure you either update your configs before pam (pam_faillock is already available"
        ewarn "and pam_pwquality will require libpwquality to be installed) or have a root session ready to apply"
        ewarn "changes with the help of eclectic config."
        ewarn "It is safe to just remove the pam_tally and pam_cracklib lines for the update."
        ewarn "It will reduce security until you've applied the new configuration but won't break your sessions."
        ewarn "Once you are ready, set PAM_UPDATE=Yes to continue the update"
        die "Ensure you have updated your PAM configs."
    fi

    if [[ ! -f /etc/pam.d/other ]]; then
        ewarn "We won't be able to run the tests. They require /etc/pam.d/other which you don't"
        ewarn "have. If you want to run the tests before installing ${PN} for the first time,"
        ewarn "copy ${FILES}/pam.d/other"
        ewarn "to /etc/pam.d/other and start the installation again. This is STRONGLY recommended."
    fi
}

pam_build_src_prepare() {
    meson_src_prepare

    # workaround to not have to depend on docbook5 (which we don't have packaged yet)
    # and also avoid to depend on some other packages
    edo cat > "${WORK}"/meson-native-pam.txt <<-EOF
[binaries]
xmlcatalog='true'
xmllint='true'
elinks='true'
w3m='true'
EOF
}

pam_build_src_test() {
    if [[ ! -f /etc/pam.d/other ]]; then
        ewarn "Skipping tests. You don't have /etc/pam.d/other."
    else
        meson_src_test
    fi
}

pam_build_src_install() {
    meson_src_install

    keepdir /etc/security/limits.d
    keepdir /etc/security/namespace.d

    # NOTE(?) needs to be suid
    edo chmod u+s "${IMAGE}"/usr/$(exhost --target)/bin/unix_chkpwd

    # install base pam configuration files
    dopamd "${FILES}"/pam.d/*

    # remove environment file that is generated by eclectic env
    edo rm "${IMAGE}"/etc/environment

    expart binaries /usr/$(exhost --target)/bin
    expart configuration /etc
    expart development /usr/$(exhost --target)/include
    expart documentation /usr/share/{doc,man}
    expart libraries /usr/$(exhost --target)/lib
}

