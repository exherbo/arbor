# Copyright 2012 Pierre Lejeune <superheron@gmail.com>
# Copyright 2021, 2023, 2024, 2025 Ali Polatel <alip@chesswob.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=seccomp release=v${PV} suffix=tar.gz ]

SUMMARY="High level interface to the Linux Kernel's seccomp filter"
DESCRIPTION="
The libseccomp library provides and easy to use, platform independent, interface to the Linux
Kernel's syscall filtering mechanism: seccomp. The libseccomp API is designed to abstract away the
underlying BPF based syscall filter language and present a more conventional function-call based
filtering interface that should be familiar to, and easily adopted by application developers.
"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-util/gperf
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-static
    --disable-python
)

src_test() {
    # Valgrind detection is "which valgrind", ie automagic.
    # We fail it here on purpose because valgrind won't work
    # under Syd. See sydbox#122.
    edo cat >"${TEMP}"/which <<'EOF'
#!/bin/bash -x
test "$1" = valgrind && exit 1
type -P "$1"
EOF
    edo chmod +x "${TEMP}"/which
    PATH="${TEMP}:${PATH}" default
}

