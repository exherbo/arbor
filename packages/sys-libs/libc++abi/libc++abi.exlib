# Copyright 2019 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require llvm-project [ pn="libcxxabi" check_target="check-cxxabi" asserts=true rtlib=true ]

export_exlib_phases src_configure src_install src_test

SUMMARY="A new implementation of low level support for a standard C++ library"

MYOPTIONS+="
    ( libc: musl )
"

DEPENDENCIES="
    build+test:
        dev-lang/clang:${LLVM_SLOT}
    post:
        sys-libs/libc++[~${PV}]
"

CMAKE_SOURCE="${WORKBASE}/llvm-project/runtimes"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_CXX_COMPILER_TARGET:STRING="$(exhost --target)"

    -DLLVM_ENABLE_RUNTIMES:STRING="libcxxabi;libcxx"
    -DLLVM_EXTERNAL_LIT:PATH="${WORKBASE}"/llvm-project/llvm/utils/lit/lit.py

    -DLIBCXX_INCLUDE_BENCHMARKS:BOOL=OFF
    -DLIBCXX_INCLUDE_TESTS:BOOL=OFF
    -DLIBCXX_ENABLE_SHARED:BOOL=ON

    -DLIBCXXABI_ENABLE_EXCEPTIONS:BOOL=ON
    -DLIBCXXABI_ENABLE_SHARED:BOOL=ON
    -DLIBCXXABI_ENABLE_STATIC:BOOL=ON
    -DLIBCXXABI_ENABLE_STATIC_UNWINDER:BOOL=OFF
    -DLIBCXXABI_ENABLE_THREADS:BOOL=ON
    -DLIBCXXABI_ENABLE_WERROR:BOOL=OFF
    -DLIBCXXABI_INCLUDE_TESTS:BOOL=ON
    -DLIBCXXABI_INSTALL_LIBRARY:BOOL=ON
    -DLIBCXXABI_INSTALL_SHARED_LIBRARY:BOOL=ON
    -DLIBCXXABI_INSTALL_STATIC_LIBRARY:BOOL=ON
    -DLIBCXXABI_STATICALLY_LINK_UNWINDER_IN_STATIC_LIBRARY:BOOL=ON
)

if ever at_least 17.0 ; then
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=ON
    )
else
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DLIBCXXABI_ENABLE_PIC:BOOL=ON
    )
fi

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'asserts LIBCXXABI_ENABLE_ASSERTIONS'
    'libc:musl LIBCXX_HAS_MUSL_LIBC'
    'providers:compiler-rt LIBCXXABI_USE_COMPILER_RT'
)

libc++abi_src_configure() {
    CC=$(exhost --tool-prefix)clang-${LLVM_SLOT} \
    CXX=$(exhost --tool-prefix)clang++-${LLVM_SLOT} \
    cmake_src_configure
}

libc++abi_src_install() {
    DESTDIR="${IMAGE}" eninja install-cxxabi
}

libc++abi_src_test() {
    llvm-project_src_test
}

