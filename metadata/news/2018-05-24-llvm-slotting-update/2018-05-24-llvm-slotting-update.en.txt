Title: LLVM is now slotted
Author: Bjorn Pagen <bjornpagen@gmail.com>
Content-Type: text/plain
Posted: 2018-05-24
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-lang/llvm

With commit a7b2f93a0df404927b33759c1263c80d415047e5, LLVM and clang are both
now slotted based on major release number, starting with version 6.0.1.
Versions of LLVM less than these versions will continue to be unslotted.
Package versions are now installed to /usr/$(exhost --target)/lib/llvm/${SLOT}/
which means parallel installations are now possible. Necessary binaries are
symlinked and alternated to /usr/$(exhost --target)/bin, based on the version
of LLVM selected in eclectic.

The [static] option has been removed for dev-lang/llvm as well. Both static
libraries and the dylib (LLVM's shared library containing all the contents of
all static libraries) are always installed now, as recommended by upstream.
